import { Router } from '@angular/router';
import { AuthService } from './../../providers/auth.service';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';

@Component({
  selector: 'app-users-edit',
  templateUrl: './users-edit.component.html',
  styleUrls: ['./users-edit.component.css']
})
export class UsersEditComponent implements OnInit {
  profileForm: FormGroup;
  userData: any = {}
  isDirty:boolean = true;
  constructor(
    private auth: AuthService,
    private router: Router
  ) { }

  currentUser = this.auth.currentUser();
 

  ngOnInit() {
    console.log(this.currentUser.token);

    let firstname = new FormControl(
      this.currentUser.user_id
    );
    let lastname = new FormControl(
      this.currentUser.username
    );
    
    this.profileForm = new FormGroup({
      firstname: firstname,
      lastname: lastname
    });

  }

  cancel() {
    this.router.navigate(['/']);
  }
}
