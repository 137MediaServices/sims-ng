import { Post } from './../../providers/post.model';
import { IProduct } from './../../providers/product.model';
import { ProductsService } from './../../providers/products.service';
import { Component, OnInit } from "@angular/core";
import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';
import { Overlay } from 'ngx-modialog'; 


@Component({
  selector: "app-products",
  styleUrls: ["./products.component.css"],
  templateUrl: "./products.component.html",
})
export class ProductsComponent implements OnInit {

  title = "Products";
  products: any;
  loaded = false;
  private product:IProduct[] = [];
  config = {
    secret_key: this.prs.config.secret_key
  };
  prod: IProduct = { 
      p_name: '',
      p_desc: ''
  };
  showModal: boolean = false;

  _form_status = {
    add: false
  };

  constructor(
    public prs: ProductsService
  ) {
   
  }

  ngOnInit(){ 
     this.getProducts();
  }

  
//   getPosts() {
//     return this.prs.postData(this.config, 'products')
//         .subscribe(
//             posts => this.products = posts['products'],
//             error => this.errorMessage = <any>error);
// }

getProducts(){
  this.prs._postData(this.config, 'products')
  .then((res) => {
   this.products = res['products'];
  })
}

updateProduct(){
  this.prod['secret_key'] = this.prs.config.secret_key;
  this.prs._postData(this.prod, 'editProduct')
  .then((res) => {
    console.log(res);
  })
}

// onClick() {
//   this.modal.alert()
//   .showClose(true)
//   .title('Hello World')
//   .body('In Angular')
//   .open();
// }


}

