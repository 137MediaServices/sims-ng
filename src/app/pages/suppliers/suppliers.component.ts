import { ProductsService } from './../../providers/products.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-suppliers',
  templateUrl: './suppliers.component.html',
  styleUrls: ['./suppliers.component.css']
})
export class SuppliersComponent implements OnInit {

  constructor(
    public _prService: ProductsService
  ) { }

  ngOnInit() {
    this._prService._checkLogin();
  }

}
