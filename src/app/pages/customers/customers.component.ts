import { ProductsService } from './../../providers/products.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-customers',
  templateUrl: './customers.component.html',
  styleUrls: ['./customers.component.css']
})
export class CustomersComponent implements OnInit {
  
  title: any = "Customers";
  constructor(
    public _prService: ProductsService
  ) { }

  ngOnInit() {
   
  }

}
