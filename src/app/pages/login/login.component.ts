import { AlertService } from './../../providers/alert.service';
import { AuthService } from './../../providers/auth.service';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  userData: any = {};
  loading = false;
  returnUrl: string;

  constructor(
    private auth: AuthService,
    private route: ActivatedRoute,
    private router: Router,
    private alertService: AlertService
  ) {
   
   } 
 

  ngOnInit() {
    this.auth.logout();
    // get return url from route parameters or default to '/'
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
    console.log(this.returnUrl);
  }

  login() {
     
    this.loading = true;
    this.auth._postData(this.userData, 'login')
        .subscribe(
            data => { 
              this.alertService.success('success');
              this.router.navigate([this.returnUrl]);
            },
            error => {
              console.log('error');
                this.alertService.error(error);
                this.loading = false;
            });
}

}
