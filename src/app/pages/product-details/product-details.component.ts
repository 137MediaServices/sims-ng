import { ProductsService } from './../../providers/products.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-product-details',
  templateUrl: './product-details.component.html',
  styleUrls: ['./product-details.component.css']
})
export class ProductDetailsComponent implements OnInit {

  id: any = this.route.snapshot.params['id'];
  config = {
    secret_key: '2a0400a50c04a5c64e7818f4920059acbedcc2a193e715b0f0310923405b36e8',
    PID: this.id
  };
  product: any[];

  constructor(
    private route: ActivatedRoute,
    private prs: ProductsService
  ) { }

  ngOnInit() {
    this._getProduct(this.id);
  }

  _getProduct(id){
    this.prs._postData(this.config, "product")
    .then((res) => {
     
      this.product = res['product'].find((res) => {
        return res.PID === id;
      });

      console.log(this.product);
    });
  }

}
