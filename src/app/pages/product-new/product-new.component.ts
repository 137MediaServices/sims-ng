import { AlertService } from './../../providers/alert.service';
import { ProductsService } from './../../providers/products.service';
import { AuthService } from './../../providers/auth.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-product-new',
  templateUrl: './product-new.component.html',
  styleUrls: ['./product-new.component.css']
})
export class ProductNewComponent implements OnInit {

  isDirty:boolean = true;
  title: any = "New Product";
  productForm: FormGroup;
  formData: any = {};

  name: FormControl;
  description: FormControl;
  category: FormControl;
  brand: FormControl;

  constructor(
    private router: Router,
    private prs: ProductsService,
    private alert: AlertService
  ) { }

  ngOnInit() {
    this.name = new FormControl(
      '', 
      [
        Validators.required,
        Validators.pattern('[a-zA-z].*')
      ]
    );
    this.description = new FormControl();
    this.category = new FormControl(
      '', Validators.required
    );
    this.brand = new FormControl(
      '',
      Validators.required
    );

    this.productForm = new FormGroup({
      p_name: this.name,
      p_desc: this.description,
      p_category_id: this.category,
      p_brand: this.brand
    });

  }

  validateName(){ 
    return this.name.invalid && this.name.touched;
  }

  validateBrand(){
    return this.brand.invalid && this.brand.touched;
  }

  saveForm(formData){ 
    if(this.productForm.valid){
      
      formData['secret_key'] = '2a0400a50c04a5c64e7818f4920059acbedcc2a193e715b0f0310923405b36e8';
      console.log(formData);
      this.prs._postData(formData, 'addProduct')
      .then((res) => {
        this.alert.success(formData.p_name +' has been added.');
        this.productForm.reset()
        console.log(res);
      })
    }
  }

  cancel() {
    this.router.navigate(['/products']);
  }

}
