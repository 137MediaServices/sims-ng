import { ProductsService } from './../../providers/products.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css']
})
export class CartComponent implements OnInit {

  constructor(
    public prs: ProductsService
  ) { }

  ngOnInit() {
    this.prs._checkLogin();
  }

}
