import { ProductsService } from './../../providers/products.service';
import { Component, ViewChild } from '@angular/core';

@Component({
  selector: 'app-shop',
  templateUrl: './shop.component.html',
  styleUrls: ['./shop.component.css']
})
export class ShopComponent {

  products: any;
  loaded = false;
  title: any = "Shop";

  config = {
    "secret_key": "2a0400a50c04a5c64e7818f4920059acbedcc2a193e715b0f0310923405b36e8"
  }

  constructor(
    public prs: ProductsService
  ) {
    this.prs._checkLogin();
    this.prs.allProducts
      .subscribe((productsResp) => {
        this.products = productsResp;
        this.loaded = true;
      });
    this.prs._products();
  }

  // _shop() {
  //   this.productService._postData(this.config, 'products')
  //     .then((res) => {
  //       this.products = res['products'];
  //       this.loaded = true;
  //     })
  // }


}
