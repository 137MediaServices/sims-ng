import { AppComponent } from './../app.component';
import { AuthService } from './../providers/auth.service';
import { ProductsService } from './../providers/products.service';
import { Component, OnInit } from "@angular/core"; 

@Component({
  selector: "app-header",
  styleUrls: ["./header.component.css"],
  templateUrl: "./header.component.html",
})
export class HeaderComponent implements OnInit {
  status: any;
  currentUser: any[];
  constructor(
    public _prService: ProductsService,
    private auth: AuthService,
    private app: AppComponent
  ) {
  }

  ngOnInit() {
    this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
  }

 


}
