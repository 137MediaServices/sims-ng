import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {

  sidebar: any = 'assets/img/sidebar-5.jpg';
  constructor() { }

  ngOnInit() {
  }

}
