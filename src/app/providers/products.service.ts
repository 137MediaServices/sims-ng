import { Post } from './post.model';
import { IProduct } from './product.model';
import { Injectable } from '@angular/core';
import { Http, Headers, Response, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';
import { Router, ActivatedRoute } from '@angular/router';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Subject } from 'rxjs/RX'; 
import {Observable} from 'rxjs/Observable';



@Injectable()
export class ProductsService {
  apiURL: string = 'http://sims.137ms.com/api/';
  
  config = {
    secret_key: '2a0400a50c04a5c64e7818f4920059acbedcc2a193e715b0f0310923405b36e8'
  };
  loaded = false;

  private productSource = new BehaviorSubject<string>('');
  allProducts = this.productSource.asObservable();

  products: any;
  doctors:any[];
  private prds = new Subject<any>();

  constructor(
    public _http: Http,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  _postData(credentials, type) {
    return new Promise((resolve) => {
      let headers = new Headers();
      let formData = JSON.stringify(credentials);

      this._http.post(this.apiURL + type, formData, { headers: headers })
        .subscribe((res) => {
          resolve(res.json());
        });
    });
  }

  _checkLogin() {
    let access = localStorage.getItem('userData');
    if (!access) {
      this.router.navigate(['/login'])
    }

  }

  _logout() {
    localStorage.clear();
    this.router.navigate(['/login']);
    console.log('clear');
  }

  _products() {
    this._postData(this.config, 'products')
      .then((res) => {
        console.log(res);
        this.products = res['products'];
        this.loaded = true;
        this.productSource.next(this.products)
      })
  }

  _getProductList():Observable<any> {
   return this._http.get('http://jsonplaceholder.typicode.com/users/')
    .map((res: Response) => { 
      console.log(res.json());
      return res.json();
    });
  }

 

  public getActivities(): Observable<any> {
    return this._http.get('https://api.github.com/users/jlooper/events')
        .map(res => res.json())
}

getProducts():Observable<IProduct[]> {
  let subject = new Subject<IProduct[]>();

  this._postData(this.config, 'products')
  .then( result => { 
      subject.next(result['products']);
      subject.complete();          
  }, (error) => {
      console.log("connection fail.");
  });

  return subject;
  
}


// API

_fetchAllProducts(){
  console.log('res');
  return this.postData(this.config, 'products');
}

postData(credentials, type):Observable<Post[]> { 
  
  // return this._http.post(apiURL+type, formData, {headers: headers})
  //     .map(this.extractData)
  //     .catch(this.handleError);
  let headers = new Headers();
  
  let body = JSON.stringify(credentials);
  return this._http.post(this.apiURL+type, body, {headers: headers} )
  .map((res: Response) => {
    return res.json()
  });

}


private extractData(res:Response) {
  let body = res.json();
  return body || [];
}

private handleError(error:any) {
  // In a real world app, we might use a remote logging infrastructure
  // We'd also dig deeper into the error to get a better message
  let errMsg = (error.message) ? error.message :
      error.status ? `${error.status} - ${error.statusText}` : 'Server error';
  console.error(errMsg); // log to console instead
  return Observable.throw(errMsg);
}


}


const PRODUCTS:IProduct[] = [ 
  {
    PID: 1,
    p_name: 'test'
  },
  {
    PID:2,
    p_name: 'TESSS'
  }
 ];