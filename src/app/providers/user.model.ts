export interface IUser {
    UID?: number,
    uname?: string,
    pwd?: string,
    fname?: string
}
