import { ProductsService } from './products.service';
import { Injectable } from '@angular/core';
import { Http, Headers, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map'
@Injectable()
export class AuthService {

  

  constructor(
    private _http: Http,
    private prs: ProductsService
  ) { } 

  apiURL = this.prs.apiURL;
  userData: any[];

  // loginUser(formData){ 
  //   this.prs._postData(formData, 'login')
  //   .then((res) => {

  //     if(res['userData']){
  //       this.userData = res['userData'];
  //       localStorage.setItem('userData', res['userData'].token);
  //       console.log(this.userData);

  //     }else if(res['error']){
  //       console.log('login error');
  //     }

  //   })
  
  // }

  authenticateUser(){
    if (localStorage.getItem('currentUser'))   
      return true;
    return false;
 
  }

  currentUser(){
    return JSON.parse(localStorage.getItem('currentUser'));
  }

  _postData(formData, type) {
    console.log(this.apiURL);
    formData = JSON.stringify(formData);
    console.log(formData);
    return this._http.post(this.apiURL+type, formData)
        .map((response: Response) => {
          
            let user = response.json();
            console.log(user);
            if (user['userData'] && user['userData'].token) {
              console.log(user['userData'].token);
              this.userData = user['userData'];
              localStorage.setItem('currentUser', JSON.stringify(user['userData']));
            }
        });
}

logout() {
    localStorage.removeItem('currentUser');
}

  // isAuthenticated(){
  //   let _userStatus = localStorage.getItem('userData');
  //   console.log(_userStatus);
  //   if(_userStatus)
  //     return true;
  //   return false;
  // }

  

}
