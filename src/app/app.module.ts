import { AuthGuard } from './guards/auth.guard';
import { AlertService } from './providers/alert.service';
import { AuthService } from './providers/auth.service';
import { SettingsComponent } from './pages/settings/settings.component';
import { ProductsService } from './providers/products.service';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppComponent } from './app.component';
import { SignupComponent } from './pages/signup/signup.component';
import { LoginComponent } from './pages/login/login.component';
import { HomepageComponent } from './pages/homepage/homepage.component';
import { ProductsComponent } from './pages/products/products.component';
import { HeaderComponent } from './header/header.component';
import { SuppliersComponent } from './pages/suppliers/suppliers.component';
import { CustomersComponent } from './pages/customers/customers.component';
import { ShopComponent } from './pages/shop/shop.component';
import { CartComponent } from './pages/cart/cart.component';
import { HttpModule } from '@angular/http';
import { LoaderComponent } from './loader/loader.component';
import { ProductDetailsComponent } from './pages/product-details/product-details.component';
import { Error404Component } from './pages/404/404.component';
import { ProductNewComponent } from './pages/product-new/product-new.component';
import { UsersComponent } from './pages/users/users.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { FooterComponent } from './footer/footer.component';
import { AlertComponent } from './pages/alert/alert.component';
import { UsersEditComponent } from './pages/users-edit/users-edit.component';

import { ModalModule } from 'ngx-modialog';
import { BootstrapModalModule } from 'ngx-modialog/plugins/bootstrap';


const appRoutes: Routes = [
  { path: 'product/new', component: ProductNewComponent, 
  canDeactivate: ['canDeactivateCreateProduct'] 
  },
  { path: '', component: DashboardComponent, canActivate: [AuthGuard] },
  { path: '404', component: Error404Component },
  { path: 'products', component: ProductsComponent, canActivate: [AuthGuard] },
  { path: 'customers', component: CustomersComponent, canActivate: [AuthGuard] },
  { path: 'suppliers', component: SuppliersComponent },
  { path: 'shop', component: ShopComponent },
  { path: 'cart', component: CartComponent },
  { path: 'login', component: LoginComponent },
  { path: 'settings', component: SettingsComponent },
  { path: 'dashboard', component: DashboardComponent },
  { path: 'products/:id', component: ProductDetailsComponent },
  { path: 'shop/:id', component: ProductDetailsComponent },
  { path: 'users/edit/:id', component: UsersEditComponent, 
  canDeactivate: ['canDeactivateUsersEdit']  },
   // otherwise redirect to home
   { path: '**', redirectTo: '' }
]

@NgModule({
  declarations: [
    AppComponent,
    SignupComponent,
    LoginComponent,
    HomepageComponent,
    ProductsComponent,
    HeaderComponent,
    DashboardComponent,
    SuppliersComponent,
    CustomersComponent,
    ShopComponent,
    CartComponent,
    LoaderComponent,
    ProductDetailsComponent,
    Error404Component,
    ProductNewComponent,
    UsersComponent,
    SidebarComponent,
    FooterComponent,
    SettingsComponent,
    AlertComponent,
    UsersEditComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forRoot(appRoutes),
    ModalModule.forRoot(),
    BootstrapModalModule
  ],
  providers: [
    ProductsService,
    AuthService,
    AlertService,
    AuthGuard,
    {
      provide: 'canDeactivateCreateProduct',
      useValue: checkDirtyState
    },
    {
      provide: 'canDeactivateUsersEdit',
      useValue: checkDirtyState_UsersEdit
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }


function checkDirtyState(component: ProductNewComponent){
  if(component.isDirty)
    return window.confirm("You have not saved this product, Do you really want to cancel?")
  return true;
}

function checkDirtyState_UsersEdit(component: UsersEditComponent){
  if(component.isDirty)
    return window.confirm("You have not saved this product, Do you really want to cancel?")
  return true;
}