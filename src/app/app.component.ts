import { AuthService } from './providers/auth.service';
import { Component } from "@angular/core";
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: "app-root",
  styleUrls: ["./app.component.css"],
  templateUrl: "./app.component.html",
})
export class AppComponent {
  title = "Products";
  isLoggedIn = false;
 

  constructor( 
    private auth: AuthService
  ) {
    
  }

}
